package com.scholastic.utils;
/**
 * 
 */


import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Class to read the the config.properties.
 * @author
 *
 */
public class MDSUtil {
	private static final String CONFIG_FILE_PATH = "src/main/resources/config/config.properties";
	private static Properties properties = new Properties();
	static {
		readConfigFile();
	}
	
	public static boolean readConfigFile(){
		try {
			properties.load(new FileInputStream(CONFIG_FILE_PATH));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * Method to get URL of GET endpoint
	 * @return
	 */
	public static String getURL(){
		return properties.getProperty("URL");
	}
	
	/**
	 * Method to get Series of GET endpoint
	 * @return
	 */
	public static List<String> getSeries(){
		String value = properties.getProperty("SERIES");
		String[] split = value.split(",");
		return Arrays.asList(split);
	}
	/**
	 * Method to get all ids seperated by comma
	 * @return
	 */
	public static List<String> getIDs(){
		String value = properties.getProperty("ID");
		String[] split = value.split(",");
		return Arrays.asList(split);
	}
	
	/**
	 * Method to get all response product ids seperated by comma
	 * @return
	 */
	public static List<String> getProductIDs(){
		String value = properties.getProperty("ProductID");
		String[] split = value.split(",");
		return Arrays.asList(split);
	}
	
	/**
	 * Method to get all response product ids seperated by comma
	 * @return
	 */
	public static String[] getImageIDs(){
		String value = properties.getProperty("ImageID");
		return value.split(";");
	}
	
	/**
	 * Method to get all response isbns seperated by comma
	 * @return
	 */
	public static List<String> getISBNs(){
		String value = properties.getProperty("ISBN");
		String[] split = value.split(",");
		return Arrays.asList(split);
	}
	
	public static List<String> getImageUrl(String imageId) {
		List<String> images = Arrays.asList(imageId.split(","));
		List<String> imagesUrls = new ArrayList<String>();
		for(int i=0; i<images.size(); i++) {
			String image = images.get(i);
			image = image.replace("[", "");
			image = image.replace("]", "");
			image = image.replace(" ", "");
			image = image.replace("", "");
			image = image.replace("\"", "");
			imagesUrls.add("http://images.pcd.prod.s3.amazonaws.com/"+image);
		}
		return imagesUrls;
	}
	
	/**
	 * Method to get all response isbns seperated by comma
	 * @return
	 */
	public static List<String> getImagesAsList(String image){
		String[] split = image.split(",");
		return Arrays.asList(split);
	}
}
