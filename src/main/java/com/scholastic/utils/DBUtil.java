package com.scholastic.utils;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

/*
 * The MongoUtil Class used to keep common methods required to access mongo DB data 
 */
public class DBUtil {
	
	// The Logger
	private final static Logger logger = Logger.getLogger(DBUtil.class);
	
	// The DBCollection
	private static DBCollection dbCollection;
	
	// The DBCollection
	private static Connection connection;
	
	/*
	 * The Mongo DB initializer
	 */
	private static DBCollection loadMongo() {
		if(dbCollection == null) {
			MongoClient mongo;
			try {
				mongo = new MongoClient(Constants.MONGO_URL, Constants.MONGO_PORT);
				DB db = mongo.getDB(Constants.MONGO_DB);
				//db.authenticate("username", "password".toCharArray());
				dbCollection = db.getCollection(Constants.MONGO_DB_COLLECTION);
			} catch (UnknownHostException e) {
				logger.error("Error while inititializing mongo db "+e.getMessage());
			}
		}
		return dbCollection;
	}
	
	/*
	 * The Mysql DB initializer
	 */
	private static Connection loadMySQL() {
		if(connection == null) {	
			try {
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection(Constants.MYSQL_URL, Constants.MYSQL_USER, Constants.MYSQL_PASSWORD);
			} catch (Exception e) {
				logger.error("Error while inititializing mysql db "+e.getMessage());
			}
		}
		return connection;
	}
	
	/*
	 * The FindByWorkId method used to find collection based on given work id
	 */
	@SuppressWarnings("rawtypes")
	public static DBObject findByWorkId(String workId) {
		loadMongo();
		
		String json1 = "{'_id': 'STG_WORK_"+workId+"'}";
			
		DBObject doc1 = (DBObject)JSON.parse(json1);
		DBObject dbObject = (DBObject) dbCollection.findOne(doc1);
		Map map = (Map) dbObject.get("values");
		String proid = (String) map.get("COVER_IMAGE_PRODUCT_ID");
		String json2 = "{'values.PCD_Product_ID':'"+proid+"'},{'values.Image_URL':1,'values.ISBN_13':1}";
		DBObject doc2 = (DBObject)JSON.parse(json2);
		dbObject = (DBObject)dbCollection.findOne(doc2);
		map = (Map) dbObject.get("values");
		return dbObject;
	}
	
	/*
	 * The FindByProductId method used to find collection based on product id
	 */
	public static DBObject findByProductId(String productId) {
		loadMongo();
		
		String json = "{_id: 'STG_P_"+productId+"'},{'values.Image_URL':1, 'values.ISBN_13':1}";
		
		DBObject dbObject = (DBObject)JSON.parse(json);
		
		return dbCollection.findOne(dbObject);
	}
	
	/*
	 * The FindByIsbn method used to find collection based on given isbn
	 */
	public static DBObject findByIsbn(String isbn) {
		loadMongo();
		
		String json = "{'values.ISBN_13': '"+isbn+"'},{'values.Image_URL':1,'values.ISBN_13':1}";
		
		DBObject dbObject = (DBObject)JSON.parse(json);
		DBCursor dbCursor = dbCollection.find(dbObject);
		while(dbCursor.hasNext()) {
			dbObject = dbCursor.next();
		}
		return dbObject;
	}
	
	/*
	 * The findBySearies method used to find data based on given series
	 */
	public static List<String> findBySearies(String series) {
		loadMySQL();
		Statement statement = null;
		List<String> workIds = null;
		try {
			statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select work_id from work where SERIES_ID = "+series);
			workIds = new ArrayList<>();
			while(resultSet.next()) {
				workIds.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			logger.error("Error while fetching data from mysql");
		}
		return workIds;
	}
}
