package com.scholastic.service.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.mongodb.DBObject;
import com.scholastic.utils.MDSUtil;
import com.scholastic.utils.DBUtil;

/*
 * The SiteServiceMongoTest Class used to automate ReST services with configured input parameters 
 */
@SuppressWarnings("rawtypes")
public class ProductSearchServiceTest {
	
	// The Logger
	private final static Logger logger = Logger.getLogger(ProductSearchServiceTest.class);
	
	/*
	 * The test initializer
	 */
	@Before
	public void setUp() {
		RestAssured.baseURI = "http://int-api.scholastic-labs.com/services/product/1.0/images";
	}

	/*
	 * Test SearchByWorkId, This method will verify Rest service response for given correct product id with actual mongo DB data
	 * @input parameter workId
	 */
	@Test
	public void testSearchByWorkId() {
		int idsSize = MDSUtil.getIDs().size();
		String id = null;
		
		for (int i = 0; i < idsSize; i++) {
			try {
				id = MDSUtil.getIDs().get(i);
				DBObject product = DBUtil.findByWorkId(id);
				logger.info("Data received from Mongodb "+product.toString());
				Map map = (Map) product.get("values");
				String images = (String) map.get("Image_URL").toString();
				List<String> imagesList = getImages(images);
				Response response =
				given().log().all().
						 pathParameters("id", id)
						.header("Accept-Encoding", "application/json").
				when()
						.get("/work/{id}").
				then()
						.statusCode(200)
						.contentType(ContentType.JSON)
						.body("_id", equalTo(product.get("_id")))
						.body("values.Image_URL.size()", equalTo(imagesList.size()))
						.body("values.ISBN_13",
								equalTo(((Map) product.get("values")).get("ISBN_13")))
						.body("values.Image_URL", equalTo(imagesList)).extract().response();

				logger.info("Service Request: " + RestAssured.baseURI + "/work/"+ id);	
				logger.info("Service Response: " + response.asString());
				
			}catch(Exception e){
				logger.error("Failed Service Request: " + RestAssured.baseURI + "/work/"+ id);
				logger.info("Service Response: " + e.getMessage());
			}
			
			logger.info("****************************************************");
		}
	}

	/*
	 * Test SearchByWorkIdForGivenIdIsInvalid, This method will verify whether Rest service response returns empty response for given wrong work id
	 * @input parameter workId
	 */
	@Test
	public void testSearchByWorkIdForGivenIdIsInvalid() {
		given().pathParameters("id", 222222)
				.header("Accept-Encoding", "application/json").when()
				.get("/work/{id}").then().statusCode(200).body(equalTo(""));
	}

	/*
	 * Test SearchByProductId, This method will verify Rest service response for given correct product id with actual mongo DB data
	 * @input parameter productId
	 */
	@Test
	public void testSearchByProductId() {
		int idsSize = MDSUtil.getIDs().size();
		String productId = null;
				
		for (int i = 0; i < idsSize; i++) {
			try {
				productId = MDSUtil.getProductIDs().get(i);
				DBObject product = DBUtil.findByProductId(productId);
				logger.info("Data received from Mongodb "+product.toString());
				Map map = (Map) product.get("values");
				String images = (String) map.get("Image_URL").toString();
				List<String> imagesList = getImages(images);
				Response response =
				given().log().all().
						 pathParameters("id", productId)
						.header("Accept-Encoding", "application/json").
				when()
						.get("/product/{id}").
				then()
						.statusCode(200)
						.contentType(ContentType.JSON)
						.body("_id", equalTo(product.get("_id")))
						.body("values.Image_URL.size()", equalTo(imagesList.size()))
						.body("values.ISBN_13",
								equalTo(((Map) product.get("values")).get("ISBN_13")))
						.body("values.Image_URL", equalTo(imagesList)).extract().response();
			
				logger.info("Service Request: " + RestAssured.baseURI + "/product/"+ productId);
				logger.info("Service Response: " + response.asString());

			}catch(Exception e){
				logger.error("Failed Service Request: " + RestAssured.baseURI + "/product/"+ productId);
				logger.info("Service Response: " + e.getMessage());
			}
			logger.info("****************************************************");
		}
	}

	/*
	 * Test SearchByProductIdForGivenIdIsInvalid, This method will verify whether Rest service response returns empty response for given wrong product id
	 * @input parameter productId
	 */
	@Test
	public void testSearchByProductIdForGivenIdIsInvalid() {
		given().pathParameters("id", 222222)
				.header("Accept-Encoding", "application/json").when()
				.get("/product/{id}").then().statusCode(200).body(equalTo(""));
	}

	/*
	 * Test SearchByISBN, This method will verify Rest service response for given correct isbn with actual mongo DB data
	 * @input parameter isbn
	 */
	@Test
	public void testSearchByISBN() {
		int idsSize = MDSUtil.getIDs().size();
		String isbn = null;
				
		for (int i = 0; i < idsSize; i++) {
			try{
				isbn = MDSUtil.getISBNs().get(i);
				
				DBObject product = DBUtil.findByIsbn(isbn);
				logger.info("Data received from Mongodb "+product.toString());
				Map map = (Map) product.get("values");
				String images = (String) map.get("Image_URL").toString();
				List<String> imagesList = getImages(images);
				
				Response response =
				given().log().all().
						 pathParameters("id", isbn)
						.header("Accept-Encoding", "application/json").
				when()
						.get("/isbn/{id}").
				then()
						.statusCode(200)
						.contentType(ContentType.JSON)
						.body("_id", equalTo(product.get("_id")))
						.body("values.Image_URL.size()", equalTo(imagesList.size()))
						.body("values.ISBN_13",
								equalTo(((Map) product.get("values")).get("ISBN_13")))
						.body("values.Image_URL", equalTo(imagesList)).extract().response();
				
				logger.info("Service Request: " + RestAssured.baseURI + "/isbn/"+ isbn);	
				logger.info("Service Response: " + response.asString());
				
			}catch(Exception e){
				logger.error("Failed Service Request: " + RestAssured.baseURI + "/isbn/"+ isbn);
				logger.info("Service Response: " + e.getMessage());
			}
			logger.info("****************************************************");
		}
	}

	/*
	 * Test SearchByISBNForGivenIsbnIsInvalid, This method will verify whether Rest service response returns empty response for given wrong isbn
	 * @input parameter isbn
	 */
	@Test
	public void testSearchByISBNForGivenIsbnIsInvalid() {
		given().pathParameters("id", 222222)
				.header("Accept-Encoding", "application/json").when()
				.get("/isbn/{id}").then().statusCode(200).body(equalTo(""));
	}
	
	private List<String> getImages(String image) {
		return MDSUtil.getImageUrl(image);
	}
}
