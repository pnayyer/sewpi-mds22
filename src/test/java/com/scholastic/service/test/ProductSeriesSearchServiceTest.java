package com.scholastic.service.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.scholastic.utils.DBUtil;
import com.scholastic.utils.MDSUtil;

/*
 * The SiteServiceMongoTest Class used to automate ReST services with configured input parameters 
 */
public class ProductSeriesSearchServiceTest {
	
	// The Logger
	private final static Logger logger = Logger.getLogger(ProductSeriesSearchServiceTest.class);
	
	/*
	 * The test initializer
	 */
	@Before
	public void setUp() {
		RestAssured.baseURI = "http://10.42.14.97:8080/ondemand";
	}

	/*
	 * Test SearchByWorkId, This method will verify Rest service response for given correct product id with actual mongo DB data
	 * @input parameter workId
	 */
	@Test
	public void testSearchByWorkId() {
		 List<String> series = MDSUtil.getSeries();
		int idsSize = series.size();
		String id = null;
		
		for (int i = 0; i < idsSize; i++) {
			try {
				id = series.get(i);
				List<String> workids = DBUtil.findBySearies(id);
				
				logger.info("Data received from mysql db "+workids);
				
				Response response =
				given().log().all().
						 pathParameters("id", id)
						.header("Accept-Encoding", "application/json").
				when()
						.get("/series/{id}").
				then()
						.statusCode(200)
						.contentType(ContentType.JSON)
						.body("references.work["+i+"].id", equalTo("STG_WORK_"+workids.get(i)))
						.body("references.work.size()", equalTo(workids.size()))
						.extract().response();

				logger.info("Service Request: " + RestAssured.baseURI + "/series/"+ id);	
				logger.info("Service Response: " + response.asString());
				
			}catch(Exception e){
				logger.error("Failed Service Request: " + RestAssured.baseURI + "/series/"+ id);
				logger.info("Service Response: " + e.getMessage());
			}
			
			logger.info("****************************************************");
		}
	}
}
